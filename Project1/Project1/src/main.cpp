#include <iostream>
#include <sstream>

#define GLEW_STATIC
#include "GL\glew.h"
#include "GLFW\glfw3.h"

using namespace std;

const char* APP_TITLE = "Introduction to OpenGL";
const int WINDOW_WIDTH = 800;
const int WINDOW_HEIGHT = 600;
bool isFullScreen = false;

// To keep things simple, we write a shader in constants
const GLchar* vertexShaderSrc =
"#version 330 core\n"
"layout (location = 0) in vec3 pos;"
"void main()"
"{"
"	gl_Position = vec4(pos.x, pos.y, pos.z, 1.0);"
"}";

const GLchar* fragmentShaderSrc =
"#version 330 core\n"
"out vec4 frag_color;"
"void main()"
"{"
"	frag_color = vec4(0.35f, 0.96f, 0.3f, 1.0f);"
"}";

// FPS
const double PHYSICS_UPDATE_FREQUENCY = 60.0;

// Input callback
void onKeyPressed(GLFWwindow* window, int key, int scanCode, int action, int mode);
void onCursorUpdated(GLFWwindow* window, double x, double y);
void onClick(GLFWwindow* window, int button, int action, int mods);

// Simulation
void gameUpdate(GLFWwindow* window, double dt);
void renderUpdate(GLFWwindow * window, double dt, GLuint shaderProgram, GLuint vao);
void physicsUpdate(GLFWwindow* window, double dt);

void showFPS(GLFWwindow* window);
//void evaluateStoredInput(GLFWwindow* window);
void drawCube() {
	GLfloat vertices[] = {
		-1, -1, -1,   -1, -1,  1,   -1,  1,  1,   -1,  1, -1,
		1, -1, -1,    1, -1,  1,    1,  1,  1,    1,  1, -1,
		-1, -1, -1,   -1, -1,  1,    1, -1,  1,    1, -1, -1,
		-1,  1, -1,   -1,  1,  1,    1,  1,  1,    1,  1, -1,
		-1, -1, -1,   -1,  1, -1,    1,  1, -1,    1, -1, -1,
		-1, -1,  1,   -1,  1,  1,    1,  1,  1,    1, -1,  1
	};

	GLfloat colors[] = {
		0, 0, 0,   0, 0, 1,   0, 1, 1,   0, 1, 0,
		1, 0, 0,   1, 0, 1,   1, 1, 1,   1, 1, 0,
		0, 0, 0,   0, 0, 1,   1, 0, 1,   1, 0, 0,
		0, 1, 0,   0, 1, 1,   1, 1, 1,   1, 1, 0,
		0, 0, 0,   0, 1, 0,   1, 1, 0,   1, 0, 0,
		0, 0, 1,   0, 1, 1,   1, 1, 1,   1, 0, 1

	};
}
int main()
{
	// Initialize glfw. Returns false if it failed to initialize.
	if (!glfwInit())
	{
		cerr << "GLFW initialization failed" << endl;
		return -1; // Returning non-zero value indicates an error
	}

	// Set opengl version to 3.3
	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
	// Set profile (compatibility (old) or core (modern) )
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
	// Set forward compatibility
	glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);

	// Create the window
	GLFWwindow* window = NULL;
	if (isFullScreen)
	{
		// To enable full screen, we must get the monitor and get the video mode for that monitor
		GLFWmonitor* monitor = glfwGetPrimaryMonitor();
		const GLFWvidmode* vidMode = glfwGetVideoMode(monitor);
		if (vidMode != NULL)
		{
			// Create window using the parameters of the video mode. We will also pass the primary monitor as parameter.
			window = glfwCreateWindow(vidMode->width, vidMode->height, APP_TITLE, monitor, NULL);
		}
	}
	else
	{
		// Not full screen, create without monitor params
		window = glfwCreateWindow(WINDOW_WIDTH, WINDOW_HEIGHT, APP_TITLE, NULL, NULL);
	}

	// Check if it failed to create the window
	if (window == NULL)
	{
		cerr << "Failed to create window" << endl;
		glfwTerminate(); // Clean up glfw
		return -1;
	}

	// Set this window as current context
	glfwMakeContextCurrent(window);

	// Set mouse and keyboard input callbacks using a function pointer.
	glfwSetCursorPosCallback(window, onCursorUpdated); // Cursor position
	glfwSetKeyCallback(window, onKeyPressed); // Key press
	glfwSetMouseButtonCallback(window, onClick); // Mouse button

												 // Initialize glew
	glewExperimental = GL_TRUE; // Setting this to true allows us to use modern OpenGL
	if (glewInit() != GLEW_OK)
	{
		cerr << "GLEW initialization failed" << endl;
		return -1;
	}
	/*
	GLfloat vertices[] = {
		0.0f, 0.5f, 0.0f, //top
		0.5f, -0.5f, 0.0f, // right
		-0.5f, -0.5f, 0.0f // left
	};
	*/
	/*
	GLfloat vertices[] = {
		///Front
	-0.5f,  0.5f, 0.0f,  //top left
	 0.5f,  0.5f, 0.0f,  //top right
	 0.5f, -0.5f, 0.0f,  //bottom right

	-0.5f, -0.5f, 0.0f,  //bottom left
	-0.5f,  0.5f, 0.0f,  //top left
	 0.5f, -0.5f, 0.0f,  //bottom right

		///Back
	-0.5f,  0.5f, -0.5f,  //top left
	 0.5f,  0.5f, -0.5f,  //top right
	 0.5f, -0.5f, -0.5f,  //bottom right

	-0.5f, -0.5f, -0.5f,  //bottom left
	-0.5f,  0.5f, -0.5f,  //top left
	 0.5f, -0.5f, -0.5f,  //bottom right
	};
	*/
	
	

	// Buffer code

	GLuint vbo; // Vertex buffer object

	glGenBuffers(1, &vbo); // Create a chunk of memory in the GPU
	glBindBuffer(GL_ARRAY_BUFFER, vbo);
	glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_STATIC_DRAW);
	// Static draw = create once, set it once, use it a lot
	// Dynamic draw = create once, change it a lot, use it a lot
	// Stream draw = create once use once

	GLuint vao; // Vertex array object
	glGenVertexArrays(1, &vao);
	glBindVertexArray(vao);

	// Tell the GPU how the vertices are laid out
	// This can be different because vertices 
	// may hold information about color and normals
	// 0 = identifier for the attribute. 
	//	Vertices has position, color and normal attributes
	// 3 = number of components in this attribute. X Y Z defines position hence 3
	// GL_FLOAT = type
	// 0 = Because of tightly packed memory, 
	//	there is no memory in between each position data
	// NULL = offset to finding the first component
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, NULL);

	// Turn on vertex array
	glEnableVertexAttribArray(0);


	////// SHADEEERRSSS ////////////
	GLint result;
	GLchar infoLog[512];

	GLuint vs = glCreateShader(GL_VERTEX_SHADER); //Type of shader
	glShaderSource(vs, 1, &vertexShaderSrc, NULL); // Copy paste shader code
												   // into opengl
	glCompileShader(vs); // Compile the shader

	glGetShaderiv(vs, GL_COMPILE_STATUS, &result);
	if (!result)
	{
		glGetShaderInfoLog(vs, sizeof(infoLog), NULL, infoLog);
		cerr << "Error! Vertex shader failed compile. " << infoLog << endl;
	}

	GLuint fs = glCreateShader(GL_FRAGMENT_SHADER);
	glShaderSource(fs, 1, &fragmentShaderSrc, NULL);
	glCompileShader(fs);

	glGetShaderiv(fs, GL_COMPILE_STATUS, &result);
	if (!result)
	{
		glGetShaderInfoLog(fs, sizeof(infoLog), NULL, infoLog);
		cerr << "Error! Fragment shader failed compile. " << infoLog << endl;
	}

	GLuint shaderProgram = glCreateProgram();
	glAttachShader(shaderProgram, vs);
	glAttachShader(shaderProgram, fs);
	glLinkProgram(shaderProgram);

	glGetProgramiv(shaderProgram, GL_LINK_STATUS, &result);
	if (!result)
	{
		glGetProgramInfoLog(shaderProgram, sizeof(infoLog), NULL, infoLog);
		cerr << "Error! Shader Program linker failure " << infoLog << endl;
	}

	glDeleteShader(vs);
	glDeleteShader(fs);
	////// END SHADEEERRSSS ////////

	// Main loop for the window
	double prevTime = 0;
	double prevFixedTime = 0;
	int frameCount = 0; // Used for fixed delta time

	glClearColor(0.23f, 0.38f, 0.47f, 1.0f);
	while (!glfwWindowShouldClose(window))
	{
		// Compute delta time between the current frame and the previous frame
		double currentTime = glfwGetTime(); // Returns the number of seconds elapsed since glfw started
		double deltaTime = currentTime - prevTime;
		prevTime = currentTime;

		// 1. Physics update (Using fixed delta time. This runs independently from other updates)
		double fixedDeltaTime = currentTime - prevFixedTime;
		if (fixedDeltaTime > 1.0 / PHYSICS_UPDATE_FREQUENCY)
		{
			prevFixedTime = currentTime;
			frameCount = 0;
			physicsUpdate(window, fixedDeltaTime);
		}

		// Joint updates (input, game logic, render)
		// 2. Input update
		glfwPollEvents(); // Poll input events

						  // 3. Game loop
		gameUpdate(window, deltaTime);

		// 4. Render loop
		// Clear the screen every frame so that we start with an empty "canvas"
		glClear(GL_COLOR_BUFFER_BIT);
		renderUpdate(window, deltaTime, shaderProgram, vao);
		// Double buffering. This will swap the front and back buffer. We might experience screen tearing if we are drawing directly on the front buffer.
		glfwSwapBuffers(window);

		// Show fps counter
		showFPS(window);
	}

	// Clean up glfw before terminating the app
	glfwTerminate();
	return 0;
}

void showFPS(GLFWwindow * window)
{
	static double previousSeconds = 0.0;
	static int frameCount = 0;

	// Compute delta time between the current frame and the previous frame
	double currentSeconds = glfwGetTime(); // Returns the number of seconds elapsed since glfw started
	double elapsedSeconds = currentSeconds - previousSeconds;

	// Update text update once every second
	if (elapsedSeconds > 1.0)
	{
		// Cache the time in this frame to compute for time difference the next time this function is called (next frame)
		previousSeconds = currentSeconds;

		// Compute for FPS
		double fps = (double)frameCount / elapsedSeconds;
		double msPerFrame = 1000.0 / fps;

		ostringstream outs;
		outs.precision(3);
		outs << fixed << APP_TITLE << "    "
			<< "FPS: " << fps << "    "
			<< "Frame Time: " << msPerFrame << " (ms)";
		glfwSetWindowTitle(window, outs.str().c_str());
		frameCount = 0;
	}

	frameCount++;
}

void onKeyPressed(GLFWwindow * window, int key, int scanCode, int action, int mode)
{
	// Check if the ESC key is pressed
	if (key == GLFW_KEY_ESCAPE && action == GLFW_PRESS)
	{
		// Close the window
		glfwSetWindowShouldClose(window, GL_TRUE);
	}
}

void onCursorUpdated(GLFWwindow * window, double x, double y)
{
	cout << "Cursor: (" << x << ", " << y << ")" << endl;
}

void onClick(GLFWwindow * window, int button, int action, int mods)
{
	if (button == GLFW_MOUSE_BUTTON_LEFT && action == GLFW_PRESS)
	{
		cout << "Left mouse button clicked" << endl;
	}
}

void gameUpdate(GLFWwindow * window, double dt)
{
	//cout << "Game update: " << dt * 1000 << "ms" << endl;
}

void renderUpdate(GLFWwindow * window, double dt,
	GLuint shaderProgram, GLuint vao)
{
	glUseProgram(shaderProgram);
	glBindVertexArray(vao);
	/*
	///Front
	glDrawArrays(GL_TRIANGLES, 0, 3);
	glDrawArrays(GL_TRIANGLES, 3, 3);
	///Back
	glDrawArrays(GL_TRIANGLES, 6, 3);
	glDrawArrays(GL_TRIANGLES, 9, 3);
	///Top
	
	///Bottom

	///Left

	///Right
	*/
	glDrawArrays(GL_TRIANGLE_FAN, 0, 4);
	glBindVertexArray(0);
}

void physicsUpdate(GLFWwindow * window, double dt)
{
	//cout << "Physics update: " << dt * 1000 << "ms" << endl;
}

//void evaluateStoredInput(GLFWwindow * window)
//{
//	// You can also get input data that was stored by the window
//
//	// Checking for key press
//	int keyState = glfwGetKey(window, GLFW_KEY_1); // This function returns GLFW_PRESS or GLFW_RELEASE
//	if (keyState == GLFW_PRESS)
//	{
//		cout << "Alpha 1 is pressed" << endl;
//	}
//
//	// Getting cursor position
//	double x, y;
//	glfwGetCursorPos(window, &x, &y);
//
//	// Checking for mouse button
//	int mouseButtonState = glfwGetMouseButton(window, GLFW_MOUSE_BUTTON_LEFT); // This function returns GLFW_PRESS or GLFW_RELEASE
//	if (keyState == GLFW_PRESS)
//	{
//		cout << "Clicked at " << " (" << x << ", " << y << ")" << endl;
//	}
//}
