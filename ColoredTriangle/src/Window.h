#pragma once
#include "GL/glew.h"
#include "GLFW/glfw3.h"
#include <iostream>
using namespace std;
class Window
{
public:
	Window(int glMajorVersion, int glMinorVersion);
	~Window();

	GLFWwindow* init();
	void setWindowWidth(int width);
	void setWindowHeight(int height);
	void setWindowTitle(char* title);
	bool getKey(int key, int action);
	void clear(float r, float b, float g, float a);
	void close();
	bool getShouldClose();
private:
	GLFWwindow* mWindow;
	int mGlMajorVersion;
	int mGlMinorVersion;
	int mWindowWidth;
	int mWindowHeight;
	char* mWindowTitle;
	bool mInitialized;
};

