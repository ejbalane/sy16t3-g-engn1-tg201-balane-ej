#include "Window.h"


Window::Window(int glMajorVersion, int glMinorVersion)
{
	mGlMajorVersion = glMajorVersion;
	mGlMinorVersion = glMinorVersion;
	mWindowWidth = 800;
	mWindowHeight = 600;
	mWindowTitle = "";
	mInitialized = false;
}

Window::~Window()
{
}

GLFWwindow * Window::init()
{
	if (!glfwInit()) // If GLFW initialize failed, w/c rarely happens. We want to exit out of the program if it happens
	{
		cerr << "GLFW initialization failed" << endl;
		return NULL; // Return value of non 0 indicates an error
	}

	// Set opengl version to 3.3
	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, mGlMajorVersion);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, mGlMinorVersion);
	// Set profile (compatibility (old) or core (modern) )
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
	// Set forward compatibility
	glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);

	// Get a pointer to a window. This is where we will render
	GLFWwindow *window = glfwCreateWindow(mWindowWidth, mWindowHeight, mWindowTitle, NULL, NULL);

	// If the window was null, something bad happened
	if (window == NULL)
	{
		cerr << "Failed to create GLFW window" << endl;
		glfwTerminate(); // You have to call this right before you exit the program
		return NULL;
	}

	glfwMakeContextCurrent(window);

	glewExperimental = GL_TRUE; // GLEW documentation tells us to set this value to true if using modern OpenGL

	if (glewInit() != GLEW_OK)
	{
		cerr << "GLEW initialization failed" << endl;
		glfwTerminate(); // You have to call this right before you exit the program
		return NULL; // Return value of non 0 indicates an error
	}

	mInitialized = true;
	mWindow = window;
	return window;
}

void Window::setWindowWidth(int width)
{
	if(mInitialized) {
		cerr << "Cannot set window width, window already initialized";
		return;
	}
	mWindowWidth = width;
}

void Window::setWindowHeight(int height)
{
	if (mInitialized) {
		cerr << "Cannot set window height, window already initialized";
		return;
	}
	mWindowHeight = height;
}

void Window::setWindowTitle(char * title)
{
	mWindowTitle = title;
}

bool Window::getKey(int key, int action)
{
	int state = glfwGetKey(mWindow, key);
	if (state == action)
		return true;
	return false;
}

void Window::clear(float r, float b, float g, float a)
{
	glClearColor(r, g, b, a);
}

void Window::close()
{
	glfwSetWindowShouldClose(mWindow, true);
}

bool Window::getShouldClose()
{
	return glfwWindowShouldClose(mWindow);
}
