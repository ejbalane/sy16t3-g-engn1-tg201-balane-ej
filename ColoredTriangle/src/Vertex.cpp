#include "Vertex.h"



Vertex::Vertex()
{
}

Vertex::Vertex(Vector3 pos)
{
	position = pos;
	color = Color::White;
}

Vertex::Vertex(Vector3 pos, Color color)
{
	this->position = pos;
	this->color = color;
}
