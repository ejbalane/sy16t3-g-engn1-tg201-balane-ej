#pragma once
#include "Drawable.h"
#include "Vertex.h"
class Mesh : Drawable
{
public:
	Mesh(Vertex vertices[], size_t vertexCount);
	~Mesh();
	
	void draw();

private:
	GLuint mVbo;
	GLuint mVboColor;
	GLuint mVao;
	size_t mSize;
};

