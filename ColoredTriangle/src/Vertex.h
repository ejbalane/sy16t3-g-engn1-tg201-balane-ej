#pragma once
#include "Vector3.h"
#include "Color.h"

class Vertex
{
public:
	Vertex();
	Vertex(Vector3 pos);
	Vertex(Vector3 pos, Color color);

	Vector3 position;
	Color color;
};

